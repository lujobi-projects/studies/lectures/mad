import numpy as np
from matplotlib import pyplot as plt

# nodes --> as long asx_data and y_data areof the same length they can take any input
x_data = np.array((0, 1, 2, 3))
y_data = np.array((1, 0, 0.3, 0))

# x_data = np.array((0, 1, 2,   3, 4, 5,   6))
# y_data = np.array((1, 0, 0.3, 0, 1, 0.3, 0))
# number of points
nr = 100


t = np.linspace(x_data[0], x_data[-1], nr)


def delta(values, i):
    """
    Calculates the delta x_{i+1} - x_i
    :param values: array of values
    :param i: index
    :return: the delta
    """
    assert 0 <= i <= len(values) - 2
    return values[i+1] - values[i]


def f_spline(x, x_val, y_val, d2f_dx2):
    """
    Calculates the function value of the spline for a given x
    :param x: position
    :param x_val: data in x direction
    :param y_val: data in y direction
    :param d2f_dx2: array of derivatives
    :return:
    """
    i = 0
    assert x_val[0] <= x <= x_val[-1]
    while i < len(x_val):
        if x_val[i] <= x:
            if i == len(x_val) - 1:
                return y_val[-1]
            if x < x_val[i+1]:
                break
        i += 1

    result = 0
    result += d2f_dx2[i]*(x_val[i+1]-x)**3/(6*delta(x_val, i))
    result += d2f_dx2[(i+1)]*((x-x_val[i])**3)/(6*delta(x_val, i))
    c = delta(y_val, i)/delta(x_val, i) - (d2f_dx2[i+1] - d2f_dx2[i])*delta(x_val, i)/6
    result += c * (x-x_val[i])
    d = y_val[i] - d2f_dx2[i]*delta(x_val, i)**2/6
    result += d

    return result


def natural_spline(x, x_val, y_val):
    """
    Calculates the natural spline for a vector
    :param x: vector of values
    :param x_val: data in x direction
    :param y_val: data in y direction
    :return: vector of values for x
    """
    assert (len(x_val) == len(y_val) and len(x_val) > 2)
    size = len(x_val)-2
    matrix = np.zeros((size, size))

    # adding A's
    for i in range(size-1):
        matrix[i+1][i] = delta(x_val, i)/6

    # adding B's
    for i in range(size):
        matrix[i][i] = (delta(x_val, i) + delta(x_val, i+1))/3

    # adding C's
    for i in range(size-1):
        matrix[i][i+1]= delta(x_val, i+1)/6

    # creating D's
    d = np.zeros(size)
    for i in range(len(d)):
        d[i] = delta(y_val, i+1)/delta(x_val, i+1)-delta(y_val, i)/delta(x_val, i)

    res_d2f_d2x = np.linalg.solve(matrix, d)
    d2f_dx2 = np.zeros(len(x_val))
    d2f_dx2[1:len(x_val)-1] = res_d2f_d2x

    y_spline = np.zeros_like(x)

    for i in range(len(y_spline)):
        y_spline[i] = f_spline(x[i], x_val, y_val, d2f_dx2)

    return y_spline


def left_clamped_spline(x, x_val, y_val):
    """
    Calculates the left clamped spline for a vector
    :param x: vector of values
    :param x_val: data in x direction
    :param y_val: data in y direction
    :return: vector of values for x
    """
    assert (len(x_val) == len(y_val) and len(x_val) > 2)
    size = len(x_val)-1
    matrix = np.zeros((size, size))

    # adding A's
    for i in range(size-1):
        matrix[i+1][i] = delta(x_val, i)/6

    # adding B's
    for i in range(size-1):
        matrix[i][i] = (delta(x_val, i) + delta(x_val, i+1))/3
    matrix[-1][-1] = delta(x_val, size-1)

    # adding C's
    for i in range(size-1):
        matrix[i][i+1] = delta(x_val, i+1)/6

    # creating D's
    d = np.zeros(size)
    for i in range(size-1):
        d[i] = delta(y_val, i+1)/delta(x_val, i+1)-delta(y_val, i)/delta(x_val, i)
    d[-1] = -delta(y_val, size-1)/delta(x_val, size-1)

    res_d2f_d2x = np.linalg.solve(matrix, d)
    d2f_dx2 = np.zeros(len(x_val))
    d2f_dx2[1:len(x_val)] = res_d2f_d2x

    y_spline = np.zeros_like(x)

    for i in range(len(y_spline)):
        y_spline[i] = f_spline(x[i], x_val, y_val, d2f_dx2)

    return y_spline


def dual_clamped_spline(x, x_val, y_val):
    """
    Calculates the on both sides clamped spline for a vector
    :param x: vector of values
    :param x_val: data in x direction
    :param y_val: data in y direction
    :return: vector of values for x
    """
    assert (len(x_val) == len(y_val) and len(x_val) > 2)
    size = len(x_val)
    matrix = np.zeros((size, size))

    # adding A's
    for i in range(size-1):
        matrix[i+1][i] = delta(x_val, i)/6

    # adding B's
    for i in range(size-2):
        matrix[i+1][i+1] = (delta(x_val, i) + delta(x_val, i+1))/3

    matrix[-1][-1] = delta(x_val, size-2)/3
    matrix[0][0] = delta(x_val, 0)/3

    # adding C's
    for i in range(size-2):
        matrix[i+1][i+2] = delta(x_val, i+1)/6
    matrix[0][1] = delta(x_val, 0)/6

    # creating D's
    d = np.zeros(size)
    for i in range(size-2):
        d[i+1] = delta(y_val, i+1)/delta(x_val, i+1)-delta(y_val, i)/delta(x_val, i)
    d[-1] = -delta(y_val, size-2)/delta(x_val, size-2)
    d[0] = delta(y_val, 0)/delta(x_val, 0)

    d2f_dx2 = np.linalg.solve(matrix, d)
    y_spline = np.zeros_like(x)

    for i in range(len(y_spline)):
        y_spline[i] = f_spline(x[i], x_val, y_val, d2f_dx2)

    return y_spline


def right_clamped_spline(x, x_val, y_val):
    """
    Calculates the right clamped spline for a vector
    :param x: vector of values
    :param x_val: data in x direction
    :param y_val: data in y direction
    :return: vector of values for x
    """
    assert (len(x_val) == len(y_val) and len(x_val) > 2)
    size = len(x_val) - 1
    matrix = np.zeros((size, size))

    # adding A's
    for i in range(size-1):
        matrix[i+1][i] = delta(x_val, i)/6

    # adding B's
    for i in range(size-1):
        matrix[i+1][i+1] = (delta(x_val, i) + delta(x_val, i+1))/3
    matrix[0][0] = delta(x_val, 0)/3

    # adding C's
    for i in range(size-2):
        matrix[i+1][i+2] = delta(x_val, i+1)/6
    matrix[0][1] = delta(x_val, 0)/6

    # creating D's
    d = np.zeros(size)
    for i in range(size-1):
        d[i+1] = delta(y_val, i+1)/delta(x_val, i+1)-delta(y_val, i)/delta(x_val, i)
    d[0] = delta(y_val, 0)/delta(x_val, 0)

    res_d2f_d2x = np.linalg.solve(matrix, d)
    d2f_dx2 = np.zeros(len(x_val))
    d2f_dx2[0:len(x_val) - 1] = res_d2f_d2x

    y_spline = np.zeros_like(x)

    for i in range(len(y_spline)):
        y_spline[i] = f_spline(x[i], x_val, y_val, d2f_dx2)

    return y_spline


# plotting
ax = plt.figure(0)
plt.xlabel('X')
plt.ylabel('Y')

plt.plot(t, natural_spline(t, x_data, y_data), 'r-', label='Natural spline')
plt.plot(t, left_clamped_spline(t, x_data, y_data), 'g-', label='Left clamped spline')
plt.plot(t, dual_clamped_spline(t, x_data, y_data), 'b-', label='Dual clamped spline')
plt.plot(t, right_clamped_spline(t, x_data, y_data), 'y-', label='Right clamped spline')
plt.plot(x_data, y_data, 'k*', label='Sample Points')
plt.grid(True)
ax.legend()

plt.show()