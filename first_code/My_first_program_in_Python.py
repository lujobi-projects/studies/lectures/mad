#!/usr/bin/python
import math
import matplotlib.pyplot as plt
import numpy as np
import random

#################################################################################
#			MAD - My first program in Python			#
#################################################################################
#			Note: 							#
#	We will use Python3 for the solutions. Which Python did you install?	#
#	to see this you can type in the terminal:				#
#	python -V								#
#	or in the python script							#
#	import sys								#
#	if (sys.version_info[0] == 3): print("Using Python3.")			#
#	elif (sys.version_info[0] == 2): print("Using Python2.")		#
#										#	
#################################################################################

#################################
# Assigning Values to Variables	# 
#################################
#notice no ; at the end of the line
a = 100          # An integer assignment
b = 1000.0       # A floating point
c = "John"       # A string

#########################
#	print function	# 
#########################
#printing a string
#for Python2: print function is without ()
print("Hello World!")

#printing a variable
print(a,b,c)

#########################
#	lists		# 
#########################
list = [ 1, 2, 3, 4, 5 ]

print("Print complete list:",list)          		 
print("Print second element of the list:",list[1])      
print("Print length of the list:",len(list))

#########################
#  array with nupy	# 
#########################
a = np.zeros(3)  	# array of length 3

#########################
#  matrix with nupy	# 
#########################
A = np.zeros((3,3))  	# (3x3) matrix 

#########################
#  	for loop	# 
#########################
#filling array with numbers
for i in range(3):
	a[i] = i
	
print("Array is: ",a)

#filling matrix with numbers
for i in range(3):
	A[i,0] = i
	A[i,1] = i+1
print("Matrix is: ",A)

#########################
#  random number	# 
#########################
randNumber = random.uniform(-1.0,1.0)
print("Random number is: ",randNumber)


#########################
#  	plotting	#
#  	line y=2x	# 
#########################
x = np.zeros(4)
y = np.zeros(4)
for i in range(4):
	x[i] = i
	y[i] = 2.0*i

#plot data
plt.figure(0)
plt.xlabel('x')
plt.ylabel('y')
plt.plot(x,y,'-b')
plt.show()
#plt.savefig("line.pdf", bbox_inches='tight')

